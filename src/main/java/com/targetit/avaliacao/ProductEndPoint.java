package com.targetit.avaliacao;

import java.lang.reflect.Array;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

@Path("/product")
public class ProductEndPoint {
	
	@Autowired
	private ProductDAO productDAO;

	@GET
	@Path("/findall")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAll() {
		List<Product> list = productDAO.findAll();
		return filterJson(list, "");
	}
	
	@GET
	@Path("/findallexprlts")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAllExclRlts() {
		List<Product> list = productDAO.findAll(); 
		return filterJson(list, "images","children","parent");
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findById(@PathParam("id") Long id) {
		Product product = productDAO.findById(id);
		return filterJson(Arrays.asList(product), "");
	}
	
	@GET
	@Path("/idexprlts/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findByIdExcludeRelation(@PathParam("id") Long id) {
		Product product = productDAO.findById(id);
		return filterJson(Arrays.asList(product), "images","children","parent");
	}
	
	@POST
	@Path("/create")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response create(Product product) {
    	productDAO.create(product);
        return Response.created(URI.create("/targetit/product/"+ product.getId())).build();
	}	
	
	@PUT
	@Path("/update")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)	
	public Response update(Product product) {
		productDAO.update(product);
		return Response.ok(product).build();
	}
	
	@DELETE
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)		
	public Response deleteArticle(@PathParam("id") Long id) {
		productDAO.delete(id);
		return Response.noContent().build();
	}	
	
	private Response filterJson(List<Product> list, String...fields ) {
		
		ObjectMapper mapper = new ObjectMapper();
		FilterProvider filters = new SimpleFilterProvider().addFilter("productFilter", 
				SimpleBeanPropertyFilter.serializeAllExcept(fields));		
		String json;
		try {
			json = mapper.writer(filters).writeValueAsString(list);			
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			json = e.getMessage();
		}
		return Response.ok(json).build();

	}
}
