package com.targetit.avaliacao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ImageDAO {

	@PersistenceContext	
	private EntityManager entityManager;	
	
	@SuppressWarnings("unchecked")
	public List<Image> findAll() {
		return entityManager.createQuery("FROM Image").getResultList();
	}
	
	public Image findById(long id) {
		return entityManager.find(Image.class, id);
	}
	
	public void create(Image image) {
		entityManager.persist(image);
	}
	
	public void update(Image image) {
		Image img = findById(image.getId());
		img.setType(image.getType());
		img.setProduct(image.getProduct());
		entityManager.flush();
	}
	
	public void delete(long id) {
		entityManager.remove(findById(id));
	}
}
