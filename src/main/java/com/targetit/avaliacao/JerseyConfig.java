package com.targetit.avaliacao;

import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
@ApplicationPath("/targetit")
public class JerseyConfig extends ResourceConfig {
	
	public JerseyConfig() {
		register(ImageEndPoint.class);
		register(ProductEndPoint.class);
	}

}
