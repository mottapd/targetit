package com.targetit.avaliacao;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonFilter;
 
@Entity
@JsonFilter("imageFilter")
public class Image {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
	
	private String type;

    @ManyToOne
    @JoinColumn(name = "PRODUCT_ID")
    private Product product;
    
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
    public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
}