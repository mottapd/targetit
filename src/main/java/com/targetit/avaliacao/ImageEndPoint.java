package com.targetit.avaliacao;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

@Path("/image")
public class ImageEndPoint {
	
	@Autowired
	private ImageDAO imageDAO;

	@GET
	@Path("/findall")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAll() {
		List<Image> list = imageDAO.findAll();
		return filterJson(list, "");
	}
	
	@GET
	@Path("/findallexprlts")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAllExclRlts() {
		List<Image> list = imageDAO.findAll(); 
		return filterJson(list, "product");
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findById(@PathParam("id") Long id) {
		Image image = imageDAO.findById(id);
		return filterJson(Arrays.asList(image), "");
	}
	
	@POST
	@Path("/create")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response create(Image image) {
		imageDAO.create(image);
        return Response.created(URI.create("/targetit/image/"+ image.getId())).build();
	}	
	
	@PUT
	@Path("/update")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)	
	public Response update(Image image) {
		imageDAO.update(image);
		return Response.ok(image).build();
	}
	
	@DELETE
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)		
	public Response deleteArticle(@PathParam("id") Long id) {
		imageDAO.delete(id);
		return Response.noContent().build();
	}	
	
	private Response filterJson(List<Image> list, String...fields ) {
		
		ObjectMapper mapper = new ObjectMapper();
		FilterProvider filters = new SimpleFilterProvider().addFilter("imageFilter", 
				SimpleBeanPropertyFilter.serializeAllExcept(fields));		
		String json;
		try {
			json = mapper.writer(filters).writeValueAsString(list);			
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			json = e.getMessage();
		}
		return Response.ok(json).build();

	}
}
