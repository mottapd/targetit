package com.targetit.avaliacao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonFilter;
 
@Entity
@JsonFilter("productFilter")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
	
	private String name;
    private String description;
    
    @OneToMany(fetch = FetchType.EAGER)
    private List<Image> images = new ArrayList<Image>();
    
    @ManyToOne
    private Product parent;
    
	@OneToMany(fetch = FetchType.EAGER, mappedBy="parent")
	private List<Product> children = new ArrayList<Product>();
    
    public Product getParent() {
		return parent;
	}
	public void setParent(Product parent) {
		this.parent = parent;
	}
	public List<Product> getChildren() {
		return children;
	}
	public void setChildren(List<Product> children) {
		this.children = children;
	}
	public List<Image> getImages() {
		return images;
	}
	public void setImages(List<Image> images) {
		this.images = images;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
    public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
    
}