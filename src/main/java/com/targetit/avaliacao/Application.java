package com.targetit.avaliacao;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
 
@SpringBootApplication
public class Application implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {
    	//Dados para teste
    	Product productA = new Product();
    	productA.setName("Radio");
    	productA.setDescription("Radio");
    	
    	Product productB = new Product();
    	productB.setName("Televisao");
    	productB.setDescription("Televisao analogica");
    	productB.setChildren(Arrays.asList(productA));
    	
    	Image image = new Image();
    	image.setType("jpg");
    	image.setProduct(productB);
    }
    
}
