package com.targetit.avaliacao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ProductDAO {

	@PersistenceContext	
	private EntityManager entityManager;	
	
	@SuppressWarnings("unchecked")
	public List<Product> findAll() {
		return entityManager.createQuery("FROM Product").getResultList();
	}
	
	public Product findById(long id) {
		return entityManager.find(Product.class, id);
	}
	
	public void create(Product product) {
		entityManager.persist(product);
	}
	
	public void update(Product product) {
		Product prod = findById(product.getId());
		prod.setDescription(product.getDescription());
		prod.setName(product.getName());
		prod.setChildren(product.getChildren());
		prod.setImages(product.getImages());
		entityManager.flush();
	}
	
	public void delete(long id) {
		entityManager.remove(findById(id));
	}
	
}
