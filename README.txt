Nome: Bruno Guimaraes Motta Vellego

Para executar a aplicacao
	mvn spring-boot:run

Console do H2
	user: admin
	sem senha
	http://localhost:8080/h2

REST
1) Create, update and delete products
  
2) Create, update and delete images

3) Get all products excluding relationships (child products, images)
	http://localhost:8080/targetit/image/findallexprlts
	http://localhost:8080/targetit/product/findallexprlts

4) Get all products including specified relationships (child product and/or images)
	http://localhost:8080/targetit/image/findall
	http://localhost:8080/targetit/product/findall

5) Same as 3 using specific product identity
	Só colocar o número do id, no exemplo id = 2
	http://localhost:8080/targetit/product/idexprlts/2

6) Same as 4 using specific product identity
	Só colocar o número do id, no exemplo id = 1
	http://localhost:8080/targetit/product/1

7) Get set of child products for specific product
8) Get set of images for specific product

REST CURL EXEMPLOS

CREATE

curl -i -v -X POST -H "Content-Type:application/json" -d '{"type":"jpg"}' localhost:8080/targetit/image/create
curl -i -v -X POST -H "Content-Type:application/json" -d '{"name":"Radio","description":"Radio analogico"}' localhost:8080/targetit/product/create

READ

curl http://localhost:8080/targetit/image/findall
curl http://localhost:8080/targetit/product/findall
curl http://localhost:8080/targetit/image/1
curl http://localhost:8080/targetit/product/1

DELETE

curl -v -X DELETE localhost:8080/targetit/image/1
curl -v -X DELETE localhost:8080/targetit/product/1

UPDATE

curl -i -v -X PUT -H "Content-Type:application/json" -d '{"id":5,"type":"bmp"}' localhost:8080/targetit/image/update
curl -i -v -X PUT -H "Content-Type:application/json" -d '{"id":8,"name":"Rasdfsddio2","description":"Raddddgico2"}' localhost:8080/targetit/product/update


